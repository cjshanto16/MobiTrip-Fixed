package com.app.mobitrip.room;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.hotel.FacilitiesAdapter;
import com.app.mobitrip.hotel.Facility;
import com.app.mobitrip.hotel.Hotel;
import com.app.mobitrip.hotel.HotelActivity;
import com.bumptech.glide.Glide;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RoomActivity extends AppCompatActivity {
    private Context mContext;
    private RecyclerView mRecyclerView;
    private FacilitiesAdapter mAdapter;
    private List<Facility> facilityList = new ArrayList<>();

    private TextView tvForPerson;
    private TextView tvRoomName;
    private TextView tvRoomDetails;
    private TextView tvRent;
    private Button btnCall;

    private String hotelId = "";
    private String roomId = "";
    private Hotel hotel;
    private Room room;
    private String phoneNumber = "";
    private int numberOfCall = 0;
    private int ii = 0;

    private SliderLayout mDemoSlider;

    private static String TAG = RoomActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        hotelId = getIntent().getStringExtra("hotel_id");
        roomId = getIntent().getStringExtra("room_id");

        mContext = RoomActivity.this;

        tvForPerson = findViewById(R.id.tvForPerson);
        tvRoomName = findViewById(R.id.tvRoomName);
        tvRoomDetails = findViewById(R.id.tvRoomDetails);
        tvRent = findViewById(R.id.tvTariffPerNight);
        btnCall = findViewById(R.id.btnCall);

        mDemoSlider = findViewById(R.id.imageSlider);
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("1", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("2", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("3", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("4", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("1",R.mipmap.room_bg);
        file_maps.put("2",R.mipmap.ads_icon);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView.image(file_maps.get(name));

            //textSliderView.image();

            //add your extra information
            //textSliderView.bundle(new Bundle());
            //textSliderView.getBundle().putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        //mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2000);

        //Init Fire base
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_hotels = database.getReference("hotels"); // call for booking count

        table_hotels.addValueEventListener(new ValueEventListener() {
            //final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //.dismiss();

                if (dataSnapshot != null) {

                    Log.d("HOTEL", "onDataChange: hotel = aaaa");

                    hotel = dataSnapshot.child(hotelId).getValue(Hotel.class);
                    Toast.makeText(mContext, "hotel data found ...", Toast.LENGTH_SHORT).show();

                    if (null != hotel) {

                        phoneNumber = hotel.getContact_number();
                        numberOfCall = hotel.getContacted();

                    }


                } else {

                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference table_rooms = database.getReference("rooms");

        table_rooms.addValueEventListener(new ValueEventListener() {

            //final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //.dismiss();

                if (dataSnapshot != null) {

                    Log.d("HOTEL", "onDataChange: hotel = rooms");

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {

                        room = dataSnapshot.child(hotelId).child(roomId).getValue(Room.class);

                        //Toast.makeText(mContext, "room data found ...", Toast.LENGTH_SHORT).show();

                        if (null != room) {

                            tvRoomName.setText(room.getRoom_name());
                            tvRoomDetails.setText(room.getRoom_details());
                            tvRent.setText(room.getRoom_rent() + " BDT");
                            tvForPerson.setText(room.getFor_person());

                        }


                    }


                } else {

                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel: " + phoneNumber));
                startActivity(intent);

                ++numberOfCall;

                ii = 0;

                table_hotels.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //.dismiss();


                        if (ii < 1) {

                            table_hotels.child(hotelId).child("contacted").setValue(numberOfCall);

                            ++ii;

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        facilityData();

        mRecyclerView = findViewById(R.id.rvFacilities);
        mAdapter = new FacilitiesAdapter(mContext, facilityList);
        mRecyclerView.setHasFixedSize(true);
        FlexboxLayoutManager mLayoutManager = new FlexboxLayoutManager(mContext);
        mLayoutManager.setFlexDirection(FlexDirection.ROW);
        mLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

    }

    private void facilityData() {

        Facility facility = new Facility();
        facility.setFacilityName("A/C");
        facilityList.add(facility);

        Facility facility2 = new Facility();
        facility2.setFacilityName("Wifi");
        facilityList.add(facility2);

        Facility facility3 = new Facility();
        facility3.setFacilityName("Smart Tv");
        facilityList.add(facility3);

        Facility facility4 = new Facility();
        facility4.setFacilityName("DTH");
        facilityList.add(facility4);

        Facility facility5 = new Facility();
        facility5.setFacilityName("Room heater");
        facilityList.add(facility5);

        Facility facility6 = new Facility();
        facility6.setFacilityName("Room service");
        facilityList.add(facility6);

    }
}
