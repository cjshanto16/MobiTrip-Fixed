package com.app.mobitrip.room;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.hotel.FacilitiesAdapter;
import com.app.mobitrip.hotel.Facility;
import com.app.mobitrip.hotel.HotelActivity;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private String TAG = RoomAdapter.class.getSimpleName();
    private List<Room> roomList;
    private String hotelId;
    FacilitiesAdapter mAdapter;
    private List<Facility> facilityList = new ArrayList<>();

    public RoomAdapter(Context context, String hotelId,List<Room> roomList) {
        this.roomList = roomList;
        this.mContext = context;
        this.hotelId = hotelId;

        facilityData();
    }

    @Override
    public RoomAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_for_rooms, parent, false);

        return new RoomAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RoomAdapter.MyViewHolder holder, final int position) {
        final Room room = roomList.get(position);

        holder.setIsRecyclable(true);

        //try {
        //Picasso.get().load(single_trip.getImage()).into(holder.bgRestaurant);

        holder.tvRoomName.setText(room.getRoom_name());
        holder.tvRent.setText(room.getRoom_rent()+" BDT");
        holder.tvRoomDetails.setText(room.getRoom_details());
        holder.tvRoomDetails.setText(room.getRoom_details());
        holder.cvSingleRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, RoomActivity.class);
                intent.putExtra("hotel_id", hotelId);
                intent.putExtra("room_id", room.getRoomId());
                mContext.startActivity(intent);
            }
        });



        mAdapter = new FacilitiesAdapter(mContext, facilityList);
        holder.mRecyclerView.setHasFixedSize(true);
        FlexboxLayoutManager mLayoutManager = new FlexboxLayoutManager(mContext);
        mLayoutManager.setFlexDirection(FlexDirection.ROW);
        mLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        holder.mRecyclerView.setLayoutManager(mLayoutManager);
        holder.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.mRecyclerView.setAdapter(mAdapter);


    }

    private void facilityData() {

        Facility facility = new Facility();
        facility.setFacilityName("A/C");
        facilityList.add(facility);

        Facility facility3 = new Facility();
        facility3.setFacilityName("Smart Tv");
        facilityList.add(facility3);

        Facility facility2 = new Facility();
        facility2.setFacilityName("Wifi");
        facilityList.add(facility2);

        Facility facility4 = new Facility();
        facility4.setFacilityName("DTH");
        facilityList.add(facility4);

    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvRoomName;
        public TextView tvRent;
        public TextView tvRoomDetails;
        public RecyclerView mRecyclerView;
        CardView cvSingleRoom;

        public MyViewHolder(View view) {
            super(view);

            tvRoomName = view.findViewById(R.id.tvRoomName);
            tvRent = view.findViewById(R.id.tvRent);
            tvRoomDetails = view.findViewById(R.id.tvRoomDetails);
            mRecyclerView = view.findViewById(R.id.rv4facilities);
            cvSingleRoom = view.findViewById(R.id.cvSingleRoom);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
