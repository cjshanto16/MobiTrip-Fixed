package com.app.mobitrip.room;

public class Room {

    private String roomId;
    private String room_name;
    private String facilities;
    private String room_rent;
    private String room_details;
    private String for_person;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getRoom_rent() {
        return room_rent;
    }

    public void setRoom_rent(String room_rent) {
        this.room_rent = room_rent;
    }

    public String getRoom_details() {
        return room_details;
    }

    public void setRoom_details(String room_details) {
        this.room_details = room_details;
    }

    public String getFor_person() {
        return for_person;
    }

    public void setFor_person(String for_person) {
        this.for_person = for_person;
    }
}
