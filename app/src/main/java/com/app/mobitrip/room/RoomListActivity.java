package com.app.mobitrip.room;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.hotel.Hotel;
import com.app.mobitrip.hotel.HotelAdapter;
import com.app.mobitrip.hotel.HotelListActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RoomListActivity extends AppCompatActivity {

    private Context mContext;
    private RecyclerView mRecyclerView;
    private RoomAdapter mAdapter;
    private List<Room> roomList = new ArrayList<>();
    private int i = 1;

    private RadioGroup rgSort;
    private RadioButton rbLTH;
    private RadioButton rbHTL;

    private String hotelId;
    private String roomId;

    private static String TAG = RoomListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_list);

        hotelId = getIntent().getStringExtra("hotel_id");
        roomId = getIntent().getStringExtra("room_id");

        mContext = RoomListActivity.this;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_rooms = database.getReference("rooms");

        final DatabaseReference table_users = database.getReference("users");
        roomList.clear();

        table_rooms.addValueEventListener(new ValueEventListener() {

            final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                int i = 1;
                if (dataSnapshot != null) {

                    for (DataSnapshot hotels : dataSnapshot.getChildren()) {

                        for (DataSnapshot rooms : hotels.getChildren()) {

                            Room room = dataSnapshot.child(hotelId).child(rooms.getKey()).getValue(Room.class);

                            Log.d(TAG, "onDataChange: hotel id = " + hotelId);
                            Log.d(TAG, "onDataChange: room id = " + rooms.getKey());

                            if (null != room) {

                                room.setRoomId(rooms.getKey());
                                roomList.add(room);
                                mAdapter.notifyDataSetChanged();

                                Log.d(TAG, "onDataChange: room name = " + room.getRoom_name());

                            }
                        }


                    }

                    Toast.makeText(mContext, "data found ...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        rgSort = findViewById(R.id.rgSort);
        rbLTH = findViewById(R.id.rbLTH);
        rbHTL = findViewById(R.id.rbHTL);

        rbLTH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //roomList.sort((o1, o2) -> o1.getRoom_name().compareTo(o2.getRoom_name()));

                Collections.sort(roomList, new Comparator<Room>() {
                    @Override
                    public int compare(Room o1, Room o2) {
                        return o1.getRoom_rent().compareTo(o2.getRoom_rent());
                    }
                });

                mAdapter.notifyDataSetChanged();
            }
        });

        rbHTL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //roomList.sort((o1, o2) -> o1.getRoom_name().compareTo(o2.getRoom_name()));

                Collections.sort(roomList, new Comparator<Room>() {
                    @Override
                    public int compare(Room o1, Room o2) {
                        return o2.getRoom_rent().compareTo(o1.getRoom_rent());
                    }
                });

                mAdapter.notifyDataSetChanged();
            }
        });



        mRecyclerView = findViewById(R.id.rvRoomList);
        mAdapter = new RoomAdapter(RoomListActivity.this, hotelId, roomList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RoomListActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }
}
