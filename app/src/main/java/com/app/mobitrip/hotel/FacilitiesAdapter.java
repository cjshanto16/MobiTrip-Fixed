package com.app.mobitrip.hotel;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;

import java.util.List;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class FacilitiesAdapter extends RecyclerView.Adapter<FacilitiesAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private String TAG = FacilitiesAdapter.class.getSimpleName();
    private List<Facility> facilityList;

    public FacilitiesAdapter(Context context, List<Facility> facilityList) {
        this.facilityList = facilityList;
        this.mContext = context;
    }

    @Override
    public FacilitiesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_facility, parent, false);

        return new FacilitiesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FacilitiesAdapter.MyViewHolder holder, final int position) {
        final Facility hotel = facilityList.get(position);

        holder.setIsRecyclable(true);

        //try {
        //Picasso.get().load(single_trip.getImage()).into(holder.bgRestaurant);

        holder.tvFacility.setText(hotel.getFacilityName());

        /*holder.tvAddress.setText(hotel.getHotel_address());
        holder.tvStartsFrom.setText("Starts from " + hotel.getMinimum_rent());
        holder.cvSingleHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, HotelActivity.class);
                intent.putExtra("hotel_id", hotel.getHotel_id());
                mContext.startActivity(intent);
            }
        });*/

        
    }

    @Override
    public int getItemCount() {
        return facilityList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvFacility;
        public TextView tvAddress;
        public TextView tvStartsFrom;
        CardView cvSingleHotel;

        public MyViewHolder(View view) {
            super(view);

            tvFacility = view.findViewById(R.id.tvFacility);

            /*tvAddress = view.findViewById(R.id.tvHotelAddress);
            tvStartsFrom = view.findViewById(R.id.tvStartsFrom);
            cvSingleHotel = view.findViewById(R.id.cvSingleHotel);*/
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
