package com.app.mobitrip.hotel;

public class Hotel {

    private String hotel_id;
    private String hotel_name;
    private String hotel_address;
    private String hotel_latitude;
    private String hotel_longitude;
    private String number_of_rooms;
    private String minimum_rent;
    private String maximum_rent;
    private String rent_range;
    private String current_discount;
    private String contact_number;
    private int contacted;

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public String getHotel_address() {
        return hotel_address;
    }

    public void setHotel_address(String hotel_address) {
        this.hotel_address = hotel_address;
    }

    public String getHotel_latitude() {
        return hotel_latitude;
    }

    public void setHotel_latitude(String hotel_latitude) {
        this.hotel_latitude = hotel_latitude;
    }

    public String getHotel_longitude() {
        return hotel_longitude;
    }

    public void setHotel_longitude(String hotel_longitude) {
        this.hotel_longitude = hotel_longitude;
    }

    public String getNumber_of_rooms() {
        return number_of_rooms;
    }

    public void setNumber_of_rooms(String number_of_rooms) {
        this.number_of_rooms = number_of_rooms;
    }

    public String getMaximum_rent() {
        return maximum_rent;
    }

    public void setMaximum_rent(String maximum_rent) {
        this.maximum_rent = maximum_rent;
    }

    public String getMinimum_rent() {
        return minimum_rent;
    }

    public void setMinimum_rent(String minimum_rent) {
        this.minimum_rent = minimum_rent;
    }

    public String getRent_range() {
        return rent_range;
    }

    public void setRent_range(String rent_range) {
        this.rent_range = rent_range;
    }

    public String getCurrent_discount() {
        return current_discount;
    }

    public void setCurrent_discount(String current_discount) {
        this.current_discount = current_discount;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public int getContacted() {
        return contacted;
    }

    public void setContacted(int contacted) {
        this.contacted = contacted;
    }
}
