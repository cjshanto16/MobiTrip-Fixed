package com.app.mobitrip.hotel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.room.Room;
import com.app.mobitrip.room.RoomListActivity;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HotelActivity extends AppCompatActivity {

    private Context mContext;
    private RecyclerView mRecyclerView;
    private FacilitiesAdapter mAdapter;
    private List<Facility> facilityList = new ArrayList<>();

    private TextView tvDiscount;
    private TextView tvHotelName;
    private TextView tvAddress;
    private TextView tvRentRange;
    private Button btnRoom;

    private String hotelId = "";
    private Hotel hotel;
    private Room room;
    private int numberOfRooms = 0;

    private SliderLayout mDemoSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel);

        mContext = HotelActivity.this;

        hotelId = getIntent().getStringExtra("hotel_id");

        Log.d("HOTEL", "onCreate: hotel id = " + hotelId);

        facilityData();

        tvDiscount = findViewById(R.id.tvDiscount);
        tvHotelName = findViewById(R.id.tvHotelName);
        tvAddress = findViewById(R.id.tvAddress);
        tvRentRange = findViewById(R.id.tvRentRange);
        btnRoom = findViewById(R.id.btnRoom);

        mDemoSlider = findViewById(R.id.imageSlider);
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "http://static2.hypable.com/wp-content/uploads/2013/12/hannibal-season-2-release-date.jpg");
        url_maps.put("Big Bang Theory", "http://tvfiles.alphacoders.com/100/hdclearart-10.png");
        url_maps.put("House of Cards", "http://cdn3.nflximg.net/images/3093/2043093.jpg");
        url_maps.put("Game of Thrones", "http://images.boomsbeat.com/data/images/full/19640/game-of-thrones-season-4-jpg.jpg");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("1",R.mipmap.logo);
        file_maps.put("2",R.mipmap.dashboard_top);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView.image(file_maps.get(name));

            //add your extra information
            //textSliderView.bundle(new Bundle());
            //textSliderView.getBundle().putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        //mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(2000);

        /*mDemoSlider.addOnPageChangeListener(this);
        ListView l = (ListView)findViewById(R.id.transformers);
        l.setAdapter(new TransformerAdapter(this));
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDemoSlider.setPresetTransformer(((TextView) view).getText().toString());
                Toast.makeText(MainActivity.this, ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });*/



        //Init Fire base
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_hotels = database.getReference("hotels");

        table_hotels.addValueEventListener(new ValueEventListener() {
            //final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //.dismiss();

                if (dataSnapshot != null) {

                    Log.d("HOTEL", "onDataChange: hotel = aaaa");

                    hotel = dataSnapshot.child(hotelId).getValue(Hotel.class);
                    Toast.makeText(mContext, "hotel data found ...", Toast.LENGTH_SHORT).show();

                    if (null != hotel) {

                        if (null != hotel.getCurrent_discount()) {
                            if (!hotel.getCurrent_discount().equalsIgnoreCase("")) {
                                tvDiscount.setText(hotel.getCurrent_discount());
                            }
                        }

                        tvHotelName.setText(hotel.getHotel_name());
                        tvAddress.setText(hotel.getHotel_address());

                    }


                } else {

                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference table_rooms = database.getReference("rooms");

        table_rooms.addValueEventListener(new ValueEventListener() {
            //final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //.dismiss();

                if (dataSnapshot != null) {

                    Log.d("HOTEL", "onDataChange: hotel = rooms");

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {

                        for (DataSnapshot dr : ds.getChildren()) {

                            room = dataSnapshot.child(hotelId).child(dr.getKey()).getValue(Room.class);

                            //Toast.makeText(mContext, "room data found ...", Toast.LENGTH_SHORT).show();

                            if (null != room) {

                                ++numberOfRooms;

                                if (numberOfRooms < 2) {

                                    btnRoom.setText("See " + numberOfRooms + " room");
                                } else {

                                    btnRoom.setText("See " + numberOfRooms + " rooms");
                                }

                                Log.d("TAG", "onDataChange: number of rooms = " + numberOfRooms);
                            }


                        }
                    }


                } else {

                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mRecyclerView = findViewById(R.id.rvFacilities);
        mAdapter = new FacilitiesAdapter(HotelActivity.this, facilityList);
        mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        FlexboxLayoutManager mLayoutManager = new FlexboxLayoutManager(mContext);
        mLayoutManager.setFlexDirection(FlexDirection.ROW);
        mLayoutManager.setJustifyContent(JustifyContent.FLEX_START);

        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HotelActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


        btnRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HotelActivity.this, RoomListActivity.class);
                intent.putExtra("hotel_id", hotelId);
                startActivity(intent);
            }
        });


    }

    private void facilityData() {

        Facility facility = new Facility();
        facility.setFacilityName("Pool");
        facilityList.add(facility);

        Facility facility2 = new Facility();
        facility2.setFacilityName("Spa");
        facilityList.add(facility2);

        Facility facility3 = new Facility();
        facility3.setFacilityName("Parking");
        facilityList.add(facility3);

        Facility facility4 = new Facility();
        facility4.setFacilityName("Wifi");
        facilityList.add(facility4);

        Facility facility5 = new Facility();
        facility5.setFacilityName("Pet");
        facilityList.add(facility5);

        Facility facility6 = new Facility();
        facility6.setFacilityName("Room service");
        facilityList.add(facility6);

        Facility facility7 = new Facility();
        facility7.setFacilityName("cafe");
        facilityList.add(facility7);

        Facility facility8 = new Facility();
        facility8.setFacilityName("Bar");
        facilityList.add(facility8);

    }

}
