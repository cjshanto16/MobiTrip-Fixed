package com.app.mobitrip.hotel;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;

import java.util.List;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private String TAG = HotelAdapter.class.getSimpleName();
    private List<Hotel> hotelList;

    public HotelAdapter(Context context, List<Hotel> hotelList) {
        this.hotelList = hotelList;
        this.mContext = context;
    }

    @Override
    public HotelAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_hotel, parent, false);

        return new HotelAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HotelAdapter.MyViewHolder holder, final int position) {
        final Hotel hotel = hotelList.get(position);

        holder.setIsRecyclable(true);

        //try {
        //Picasso.get().load(single_trip.getImage()).into(holder.bgRestaurant);

        holder.tvHotelName.setText(hotel.getHotel_name());
        holder.tvAddress.setText(hotel.getHotel_address());
        holder.tvStartsFrom.setText("Starts from " + hotel.getMinimum_rent());
        holder.cvSingleHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, HotelActivity.class);
                intent.putExtra("hotel_id", hotel.getHotel_id());
                Log.d(TAG, "onClick: hotel-id = "+hotel.getHotel_id());
                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return hotelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvHotelName;
        public TextView tvAddress;
        public TextView tvStartsFrom;
        CardView cvSingleHotel;

        public MyViewHolder(View view) {
            super(view);

            tvHotelName = view.findViewById(R.id.tvHotelName);
            tvAddress = view.findViewById(R.id.tvHotelAddress);
            tvStartsFrom = view.findViewById(R.id.tvStartsFrom);
            cvSingleHotel = view.findViewById(R.id.cvSingleHotel);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
