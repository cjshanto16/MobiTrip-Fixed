package com.app.mobitrip.hotel;

public class Facility {

    private String facilityName;
    private int facilityIcon;

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public int getFacilityIcon() {
        return facilityIcon;
    }

    public void setFacilityIcon(int facilityIcon) {
        this.facilityIcon = facilityIcon;
    }
}
