package com.app.mobitrip.hotel;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.model.TripMember;
import com.app.mobitrip.model.User;
import com.app.mobitrip.new_trip.NewTripActivity;
import com.app.mobitrip.trips.TripAdapter;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;



public class HotelListActivity extends AppCompatActivity {

    private Context mContext;
    private RecyclerView mRecyclerView;
    private HotelAdapter mAdapter;
    private List<Hotel> hotelList = new ArrayList<>();
    private int i = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_list);

        mContext = HotelListActivity.this;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_hotel = database.getReference("hotels");

        final DatabaseReference table_users = database.getReference("users");
        hotelList.clear();

        table_hotel.addValueEventListener(new ValueEventListener() {

            final Dialog mDialog = ProgressDialog.show(HotelListActivity.this, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                int i = 1;
                if (dataSnapshot != null) {
                    for (DataSnapshot hotels : dataSnapshot.getChildren()) {

                        //Log.d(TAG, "onDataChange: allUsers = " + allUsers.getKey());

                        Hotel hotel = dataSnapshot.child(hotels.getKey()).getValue(Hotel.class);

                        if (null != hotel) {

                            hotel.setHotel_id(hotels.getKey());
                            hotelList.add(hotel);
                            mAdapter.notifyDataSetChanged();


                        }
                        //Log.d(TAG, "onDataChange: restaurant ===================================== ");
                    }

                    Toast.makeText(HotelListActivity.this, "data found ...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(HotelListActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mRecyclerView = findViewById(R.id.rvHotelList);
        mAdapter = new HotelAdapter(HotelListActivity.this, hotelList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HotelListActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }
}
