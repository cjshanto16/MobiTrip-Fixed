package com.app.mobitrip.weather;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Temperature implements Serializable {

    @SerializedName("temp")
    private String temp;

    @SerializedName("description")
    private String description;

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
