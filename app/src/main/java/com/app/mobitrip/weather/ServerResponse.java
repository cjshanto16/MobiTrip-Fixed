package com.app.mobitrip.weather;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServerResponse implements Serializable {

    @SerializedName("main")
    private Temperature temperature;

    @SerializedName("weather")
    private Temperature[] weather;

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Temperature[] getWeather() {
        return weather;
    }

    public void setWeather(Temperature[] weather) {
        this.weather = weather;
    }
}
