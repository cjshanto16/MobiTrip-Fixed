package com.app.mobitrip.weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("weather")
    Call<ServerResponse> getWeather(
            @Query("q") String location,
            @Query("units") String units,
            @Query("appid") String appId
    );

}
