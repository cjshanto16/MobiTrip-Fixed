package com.app.mobitrip.trips.expense;

import com.google.firebase.database.DataSnapshot;

public interface ExpenseInterface {

    void onRemoveMyExpense(DataSnapshot dataSnapshot);

}
