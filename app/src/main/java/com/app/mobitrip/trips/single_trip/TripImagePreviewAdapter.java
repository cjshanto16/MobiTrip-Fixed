package com.app.mobitrip.trips.single_trip;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;

import java.util.List;

public class TripImagePreviewAdapter extends RecyclerView.Adapter<TripImagePreviewAdapter.ViewHolder>{

    public List<Uri> fileList;


    public TripImagePreviewAdapter(List<Uri> fileList) {
        this.fileList = fileList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_preview, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.fileDoneView.setImageURI(fileList.get(position));

    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public ImageView fileDoneView;

        public ViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            fileDoneView =  mView.findViewById(R.id.upload_icon);


        }

    }

}
