package com.app.mobitrip.trips.single_trip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Expense;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.model.User;
import com.app.mobitrip.trips.expense.MyExpenseAdapter;
import com.app.mobitrip.trips.expense.TeamExpenseActivity;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.app.mobitrip.new_trip.NewTripActivity.userList;

public class TripActivity extends AppCompatActivity implements OnMapReadyCallback {

    FirebaseDatabase database;

    private String tripId;
    private Context mContext;
    private Trip trip;
    private String current_user;
    private String host;
    private boolean isHost = false;

    private TextView tvTripName;
    private TextView tvStartPoint;
    private TextView tvEndPoint;
    private TextView tvStartDate;
    private TextView tvTime;
    private TextView tvAmountPerPerson;
    private TextView tvCommonExpense;

    private TextView mapFrom;
    private TextView mapTo;

    /*Trip Share Button*/
    private ImageView tripShareButton;

    private Button btnStartTrip;
    /*Team Expense Recycler View*/
    private RecyclerView tripExpenseRV;
    DatabaseReference table_expenses;


    public static List<Expense> teamExpenses = new ArrayList<>();
    private static int teamTotalExpense = 0;

    /*Recyclerview Adapter for firebase*/
    private MyExpenseAdapter teamExpenseAdapter;

    private int ii = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        mContext = this;
        tripId = getIntent().getStringExtra("trip_id");

        if (null == tripId) {
            Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            finish();
        }



        tvTripName = findViewById(R.id.tvTripName);
        tvStartPoint = findViewById(R.id.tvStartPoint);
        tvEndPoint = findViewById(R.id.tvEndPoint);
        tvStartDate = findViewById(R.id.tvStartDate);
        tvTime = findViewById(R.id.tvTime);
        tvAmountPerPerson = findViewById(R.id.amount_per);
        tvCommonExpense = findViewById(R.id.common_expense);
        btnStartTrip = findViewById(R.id.btnStartTrip);
        tripExpenseRV = findViewById(R.id.trip_team_expensesRV);

        mapFrom = findViewById(R.id.from_map);
        mapTo = findViewById(R.id.to_map);

        tripShareButton = findViewById(R.id.trip_share_btn);

        //Init Fire base
        database = FirebaseDatabase.getInstance();
        final DatabaseReference table_trips = database.getReference("trips");
        table_trips.addValueEventListener(new ValueEventListener() {
            //final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //.dismiss();

                if (dataSnapshot != null) {

                    //Log.d(TAG, "onDataChange: allUsers = " + allUsers.getKey());

                    trip = dataSnapshot.child(tripId).getValue(Trip.class);

                    if (trip.getTrip_status().equalsIgnoreCase("completed"))
                    {
                        tripShareButton.setVisibility(View.VISIBLE);
                    }else
                    {
                        tripShareButton.setVisibility(View.GONE);
                    }

                    tvTripName.setText(trip.getTrip_name());
                    tvStartPoint.setText(trip.getStart_point());
                    tvEndPoint.setText(trip.getEnd_point());
                    tvStartDate.setText(trip.getDate());
                    tvTime.setText(trip.getTime());
                    tvAmountPerPerson.setText(trip.getAmount_per_person());
                    tvCommonExpense.setText(trip.getCommon_expense());

                    /*set map data*/
                    mapFrom.setText(trip.getStart_point());
                    mapTo.setText(trip.getEnd_point());

                    current_user = PreferenceUtils.getPreference(mContext, "USER_NAME");
                    host = trip.getCreated_by();

                    if (host.equalsIgnoreCase(current_user)) {
                        isHost = true;
                        btnStartTrip.setVisibility(View.VISIBLE);
                        String currentlyOnATrip = PreferenceUtils.getPreference(mContext, Constant.CURRENTLY_ON_A_TRIP);
                        if (null != currentlyOnATrip) {
                            if (currentlyOnATrip.equalsIgnoreCase(Constant.YES)) {
                                btnStartTrip.setBackground(getResources().getDrawable(R.drawable.inactive));
                            } else {
                                btnStartTrip.setBackground(getResources().getDrawable(R.drawable.save_trip));
                            }
                        }
                    } else {
                        isHost = false;
                        btnStartTrip.setVisibility(View.GONE);
                    }

                } else {

                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final DatabaseReference table_users = database.getReference("users");
        userList.clear();
        table_users.addValueEventListener(new ValueEventListener() {

            final Dialog mDialog = ProgressDialog.show(TripActivity.this, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                int i = 1;
                if (dataSnapshot != null) {
                    for (DataSnapshot allUsers : dataSnapshot.getChildren()) {

                        //Log.d(TAG, "onDataChange: allUsers = " + allUsers.getKey());
                        User user = dataSnapshot.child(allUsers.getKey()).getValue(User.class);

                        if (null != user) {

                            //user.setId(allUsers.getKey());
                            userList.add(user);


                        }
                        //Log.d(TAG, "onDataChange: restaurant ===================================== ");
                    }

                    Toast.makeText(TripActivity.this, "data found ...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TripActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        findViewById(R.id.cvTravelMates).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripActivity.this, TravelMatesActivity.class);
                intent.putExtra("trip_id", tripId);
                intent.putExtra("host", isHost);
                startActivity(intent);

            }
        });

        btnStartTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentlyOnATrip = PreferenceUtils.getPreference(mContext, Constant.CURRENTLY_ON_A_TRIP);
                if (null != currentlyOnATrip) {
                    if (currentlyOnATrip.equalsIgnoreCase(Constant.YES)) {
                        Toast.makeText(mContext, "Failed! You're currently on another trip.", Toast.LENGTH_SHORT).show();
                    } else {
                        PreferenceUtils.savePreference(mContext, Constant.CURRENTLY_ON_A_TRIP, Constant.YES);
                        PreferenceUtils.savePreference(mContext, Constant.CURRENT_TRIP, tripId);

                        ii = 0;

                        table_trips.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                if (dataSnapshot != null) {

                                    if (ii < 1) {
                                        table_trips.child(tripId).child("trip_status").setValue(Constant.CURRENT);
                                        ++ii;
                                    }

                                } else {
                                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        Toast.makeText(mContext, "Trip started successfully!", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } else {
                    PreferenceUtils.savePreference(mContext, Constant.CURRENTLY_ON_A_TRIP, Constant.YES);
                    PreferenceUtils.savePreference(mContext, Constant.CURRENT_TRIP, tripId);

                    ii = 0;

                    table_trips.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot != null) {

                                if (ii < 1) {
                                    table_trips.child(tripId).child("trip_status").setValue(Constant.CURRENT);
                                    ++ii;
                                }

                            } else {
                                Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    Toast.makeText(mContext, "Trip started successfully!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });


        /*Team expense list showing */
        teamExpenseAdapter = new MyExpenseAdapter(TripActivity.this, teamExpenses, false);
        LinearLayoutManager mManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        tripExpenseRV.setLayoutManager(mManager);
        tripExpenseRV.setHasFixedSize(true);
        tripExpenseRV.setItemAnimator(new DefaultItemAnimator());
        tripExpenseRV.setAdapter(teamExpenseAdapter);

        /*Initialize expenses database table*/
        table_expenses = database.getReference("expenses");
        DatabaseReference teamExpenseList = table_expenses.child(tripId);

        teamExpenseList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                teamTotalExpense = 0;
                teamExpenses.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    String user = ds.getKey();

                    int mTotalExpense = 0;
                    for (DataSnapshot dp : ds.getChildren()) {

                        Expense expense = ds.child(dp.getKey()).getValue(Expense.class);

                        if (expense != null) {
                            //Toast.makeText(mContext, "user now = " + PreferenceUtils.getPreference(mContext, "USER_NAME"), Toast.LENGTH_SHORT).show();

                            mTotalExpense += Integer.parseInt(expense.getAmount());


                        }


                    }

                    Expense userExpense = new Expense();
                    userExpense.setSpentOn(user);
                    userExpense.setAmount(mTotalExpense + "");

                    teamTotalExpense += mTotalExpense;
                    teamExpenses.add(userExpense);
                    teamExpenseAdapter.notifyDataSetChanged();
                }
                //Toast.makeText(mContext, dataSnapshot.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        tripShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TripActivity.this,TripShareActivity.class);
                intent.putExtra("TRIP_ID",tripId);
                intent.putExtra("TRIP_NAME",trip.getTrip_name());
                startActivity(intent);
            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
