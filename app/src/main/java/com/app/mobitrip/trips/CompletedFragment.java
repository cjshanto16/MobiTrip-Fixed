package com.app.mobitrip.trips;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.model.TripMember;
import com.app.mobitrip.trips.current_trip.CurrentTripActivity;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CompletedFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private View view;
    private Context mContext;

    //private RelativeLayout rlCurrentTrip;
    private RecyclerView mRecyclerView;
    private TripAdapter mAdapter;
    //private CardView cvCurrentTrip;

    private List<Trip> completedTrips = new ArrayList<>();
    public static List<TripMember> invited = new ArrayList<>();
    private Trip trip;
    int i = 1;

    private String TAG = CompletedFragment.class.getSimpleName();

    public CompletedFragment() {
        // Required empty public constructor
    }

    public static CompletedFragment newInstance(String param1, String param2) {
        CompletedFragment fragment = new CompletedFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_completed, container, false);
        mContext = getActivity();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        final String current_user = PreferenceUtils.getPreference(mContext, "USER_NAME");
        mRecyclerView = view.findViewById(R.id.rvCompletedTrips);

        //Init Firebase
        // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        //final DatabaseReference table_mates = database.getReference("trip_members");

        invited.clear();
        //FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_trips = database.getReference("trips");
        table_trips.addValueEventListener(new ValueEventListener() {

            final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();

                if (dataSnapshot != null && i < 2) {

                    completedTrips.clear();
                    //hostedTrips.clear();

                    for (final DataSnapshot singleTrip : dataSnapshot.getChildren()) {

                        //Log.d(TAG, "onDataChange: allUsers = " + allUsers.getKey());

                        Trip trip = singleTrip.getValue(Trip.class);

                        if (null != trip) {

                            //final String current_user = PreferenceUtils.getPreference(mContext, Constant.USER_NAME);

                            trip.setTrip_id(singleTrip.getKey());
                            //allTrips.add(trip);

                            if (trip.getCreated_by().equalsIgnoreCase(current_user)) {
                                //Toast.makeText(mContext, "user now = " + PreferenceUtils.getPreference(mContext, Constant.USER_NAME), Toast.LENGTH_SHORT).show();

                                if (trip.getTrip_status().equalsIgnoreCase(Constant.COMPLETED)) {

                                    completedTrips.add(trip);
                                    mAdapter.notifyDataSetChanged();
                                    Log.d(TAG, "onDataChange: It's my U trip -> " + trip.getTrip_name());

                                } /*else if (trip.getTrip_status().equalsIgnoreCase(Constant.CURRENT)) {
                                    PreferenceUtils.savePreference(mContext, Constant.CURRENTLY_ON_A_TRIP, Constant.YES);
                                    PreferenceUtils.savePreference(mContext, Constant.CURRENT_TRIP, trip.getTrip_id());

                                    Log.d(TAG, "onDataChange: It's my C trip -> " + trip.getTrip_name());
                                }*/
                            } else {

                                if (null != trip.getTrip_members()) {

                                    /*List<TripMember> tripMemberList = trip.getTrip_members();
                                    for (TripMember member : tripMemberList) {*/
                                    if (trip.getTrip_members().contains(current_user)) {
                                        if (trip.getTrip_status().equalsIgnoreCase(Constant.COMPLETED)) {

                                            Log.d(TAG, "onDataChange: U. It's me -> " + current_user);
                                            completedTrips.add(trip);
                                            mAdapter.notifyDataSetChanged();

                                        } /*else if (trip.getTrip_status().equalsIgnoreCase(Constant.CURRENT)) {
                                            PreferenceUtils.savePreference(mContext, Constant.CURRENTLY_ON_A_TRIP, Constant.YES);
                                            PreferenceUtils.savePreference(mContext, Constant.CURRENT_TRIP, trip.getTrip_id());
                                            Log.d(TAG, "onDataChange: C. It's me -> " + current_user);
                                        }*/
                                    } else {
                                        Log.d(TAG, "onDataChange: It's not me.");
                                    }
                                }

                                /*} else {
                                    Log.d(TAG, "onDataChange: no member in " + trip.getTrip_name());
                                }*/

                                Log.d(TAG, "onDataChange: It's not me. It is " + trip.getCreated_by() + "'s trip.");
                            }
                        }

                        i++;
                    }

                    //Toast.makeText(mContext, "data found ...", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mAdapter = new TripAdapter(mContext, completedTrips);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        /*cvCurrentTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CurrentTripActivity.class);
                startActivity(intent);
            }
        });*/

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
