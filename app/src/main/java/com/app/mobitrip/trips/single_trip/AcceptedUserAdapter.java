package com.app.mobitrip.trips.single_trip;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.model.User;

import java.util.List;

import static com.app.mobitrip.new_trip.NewTripActivity.userList;
import static com.app.mobitrip.trips.single_trip.TravelMatesActivity.acceptedUsers;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class AcceptedUserAdapter extends RecyclerView.Adapter<AcceptedUserAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private boolean isHost;
    private String TAG = AcceptedUserAdapter.class.getSimpleName();
    private List<User> restaurantList;

    public AcceptedUserAdapter(Context context, List<User> restaurantList, boolean isHost) {
        this.restaurantList = restaurantList;
        this.mContext = context;
        this.isHost = isHost;
    }

    @Override
    public AcceptedUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_user_row, parent, false);

        return new AcceptedUserAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AcceptedUserAdapter.MyViewHolder holder, final int position) {
        final User userListView = restaurantList.get(position);

        holder.setIsRecyclable(false);

        //try {
        //Picasso.get().load(userListView.getImage()).into(holder.bgRestaurant);


        //holder.restaurantName.setText(userListView.getRestaurant_name());

        for (User user : userList) {

            if (user.getUsername().equalsIgnoreCase(userListView.getName())) {
                holder.tvName.setText(user.getName());
                break;
            }
        }

        // to-do isHost
        if(!isHost) holder.imvRemove.setVisibility(View.GONE);

        holder.imvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptedUsers.remove(userListView);
                notifyDataSetChanged();
                // todo delete user from invitation list
            }
        });


    }

    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvName;
        ImageView imvAvatar;
        ImageView imvRemove;

        public MyViewHolder(View view) {
            super(view);

            tvName = view.findViewById(R.id.tvName);
            imvAvatar = view.findViewById(R.id.imvAvatar);
            imvRemove = view.findViewById(R.id.imvRemove);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
