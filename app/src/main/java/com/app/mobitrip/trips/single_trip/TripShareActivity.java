package com.app.mobitrip.trips.single_trip;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.main.MainActivity;
import com.app.mobitrip.model.Feed;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TripShareActivity extends AppCompatActivity {

    private static final int PICK_IMG = 1;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private ArrayList<Uri> ImageList = new ArrayList<Uri>();
    private int uploads = 0;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    int index = 0;

    EditText tripName,tripDetails,hotelName,busName;
    ImageView choose;
    Button shareFeedBtn;

    RecyclerView imagePreviewRV;
    TripImagePreviewAdapter previewAdapter;

    String tripId;
    String shareId;
    String tripNameTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_share);

        if (getIntent().getExtras()!= null)
        {
            tripId = getIntent().getExtras().getString("TRIP_ID");
            tripNameTxt = getIntent().getExtras().getString("TRIP_NAME");
        }

        databaseReference = FirebaseDatabase.getInstance().getReference().child("feed");
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please wait");
        progressDialog.setMessage("Uploading ..........");
        progressDialog.setCanceledOnTouchOutside(false);
        choose = findViewById(R.id.add_images);
        imagePreviewRV = findViewById(R.id.tripImagesRV);
        tripName = findViewById(R.id.tripName);
        hotelName = findViewById(R.id.hotelName);
        busName = findViewById(R.id.busName);
        tripDetails = findViewById(R.id.postDetails);
        shareFeedBtn = findViewById(R.id.shareFeedBtn);

        //set trip name
        tripName.setText(tripNameTxt);

        choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= 23)
                {
                    if (checkPermission())
                    {
                        // Code for above or equal 23 API Oriented Device
                        // Your Permission granted already .Do next code
                        choose();
                    } else {
                        requestPermission(); // Code for permission
                    }
                }
                else
                {
                    choose();

                    // Code for Below 23 API Oriented Device
                    // Do next code
                }

            }
        });

        shareFeedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                shareId = databaseReference.push().getKey();

                Feed feed = new Feed();
                feed.setTrip_id(tripId);
                feed.setTrip_name(tripName.getText().toString());
                feed.setTrip_details(tripDetails.getText().toString());
                feed.setBus_name(busName.getText().toString());
                feed.setHotel_name(hotelName.getText().toString());


                databaseReference.child(shareId).setValue(feed).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful())
                        {
                            upload();
                        }
                    }
                });

            }
        });

    }

    public void choose() {
        //we will pick images
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, PICK_IMG);

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMG) {
            if (resultCode == RESULT_OK) {
                if (data.getClipData() != null) {
                    int count = data.getClipData().getItemCount();

                    int CurrentImageSelect = 0;

                    while (CurrentImageSelect < count) {
                        Uri imageuri = data.getClipData().getItemAt(CurrentImageSelect).getUri();
                        ImageList.add(imageuri);
                        CurrentImageSelect = CurrentImageSelect + 1;
                    }
                    previewAdapter = new TripImagePreviewAdapter(ImageList);
                    imagePreviewRV.setLayoutManager(new LinearLayoutManager(TripShareActivity.this,RecyclerView.HORIZONTAL,false));
                    imagePreviewRV.setAdapter(previewAdapter);
                    previewAdapter.notifyDataSetChanged();
                }

            }

        }

    }


    public void upload() {

        if (TextUtils.isEmpty(tripName.getText().toString()))
        {
            tripName.setError("Trip name can't be empty");
            tripName.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(tripDetails.getText().toString()))
        {
            tripDetails.setError("Please enter a trip details");
            tripDetails.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(hotelName.getText().toString()))
        {
            hotelName.setError("Hotel name can't be empty");
            hotelName.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(busName.getText().toString()))
        {
            busName.setError("Bus name can't be empty");
            busName.requestFocus();
            return;
        }
        progressDialog.show();
        final StorageReference ImageFolder =  FirebaseStorage.getInstance().getReference();
        for (uploads=0; uploads < ImageList.size(); uploads++) {
            Uri Image  = ImageList.get(uploads);
            final StorageReference imagename = ImageFolder.child(Image.getLastPathSegment());

            imagename.putFile(ImageList.get(uploads)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    imagename.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {

                            String url = String.valueOf(uri);
                            SendLink(url);
                        }
                    });

                }
            });


        }


    }

    private void SendLink(String url) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("image", url);

        databaseReference.child(shareId).child("images").push().setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressDialog.dismiss();
                ImageList.clear();

                Intent intent = new Intent(TripShareActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(TripShareActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(TripShareActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(TripShareActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(TripShareActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                    choose();
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
}
