package com.app.mobitrip.trips.expense;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Expense;
import com.app.mobitrip.model.User;

import java.util.List;

import static com.app.mobitrip.new_trip.NewTripActivity.userList;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class TeamExpenseAdapter extends RecyclerView.Adapter<TeamExpenseAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private boolean isHost;
    private String TAG = TeamExpenseAdapter.class.getSimpleName();
    private List<Expense> expenseList;

    public TeamExpenseAdapter(Context context, List<Expense> expenseList) {
        this.expenseList = expenseList;
        this.mContext = context;
    }

    @Override
    public TeamExpenseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_expense_model, parent, false);

        return new TeamExpenseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TeamExpenseAdapter.MyViewHolder holder, final int position) {
        final Expense expenseListView = expenseList.get(position);

        holder.setIsRecyclable(false);

        //try {
        //Picasso.get().load(expenseListView.getImage()).into(holder.bgRestaurant);


        holder.tvAmount.setText(expenseListView.getAmount());

        for (User user : userList) {

            if (user.getUsername().equalsIgnoreCase(expenseListView.getSpentOn())) {
                holder.tvSpentOn.setText(user.getName());
                break;
            }
        }


    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvSpentOn;
        public TextView tvAmount;

        public MyViewHolder(View view) {
            super(view);

            tvSpentOn = view.findViewById(R.id.tvSpentOn);
            tvAmount = view.findViewById(R.id.tvAmount);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
