package com.app.mobitrip.trips.expense;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Expense;

import java.util.List;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class MyExpenseAdapter extends RecyclerView.Adapter<MyExpenseAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private boolean isHost;
    private String TAG = MyExpenseAdapter.class.getSimpleName();
    private List<Expense> expenseList;
    private boolean isMyExpense = false;

    public MyExpenseAdapter(Context context, List<Expense> expenseList, boolean isMyExpense) {
        this.expenseList = expenseList;
        this.mContext = context;
        this.isMyExpense = isMyExpense;
    }

    @Override
    public MyExpenseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_expense_model, parent, false);

        return new MyExpenseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyExpenseAdapter.MyViewHolder holder, final int position) {
        final Expense expenseListView = expenseList.get(position);

        holder.setIsRecyclable(false);

        holder.tvSpentOn.setText(expenseListView.getSpentOn());
        holder.tvAmount.setText(expenseListView.getAmount());

        /*for (Expense user : userList) {

            if (user.getUsername().equalsIgnoreCase(expenseListView.getName())) {
                holder.tvName.setText(user.getName());
                break;
            }
        }*/

        holder.imvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expenseListView.getDataSnapshot().getRef().removeValue();
                Toast.makeText(mContext, "Expense is removed.", Toast.LENGTH_SHORT).show();
            }
        });

        /*holder.imvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expenseListView.getDataSnapshot().getRef().removeValue();
                Toast.makeText(mContext, "Expense is removed.", Toast.LENGTH_SHORT).show();
            }
        });*/


    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvSpentOn;
        public TextView tvAmount;
        public ImageView imvRemove;
        public ImageView imvEdit;
        public LinearLayout llExpense;

        public MyViewHolder(View view) {
            super(view);

            tvSpentOn = view.findViewById(R.id.tvSpentOn);
            tvAmount = view.findViewById(R.id.tvAmount);
            imvRemove = view.findViewById(R.id.imvRemove);
            imvEdit = view.findViewById(R.id.imvEdit);
            llExpense = view.findViewById(R.id.llExpense);

            if (isMyExpense) {
                imvRemove.setVisibility(View.VISIBLE);
                //imvEdit.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
