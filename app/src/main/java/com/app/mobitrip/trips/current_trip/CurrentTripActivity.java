package com.app.mobitrip.trips.current_trip;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.mobitrip.R;
import com.app.mobitrip.trips.expense.MyExpenseActivity;
import com.app.mobitrip.trips.expense.TeamExpenseActivity;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.app.mobitrip.weather.ApiInterface;
import com.app.mobitrip.weather.RetrofitApiClient;
import com.app.mobitrip.weather.ServerResponse;
import com.app.mobitrip.weather.Temperature;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentTripActivity extends AppCompatActivity implements LocationListener {

    private TextView tvCurrentLocation;
    private TextView tvTemp;
    private TextView tvWeather;
    private Button btnMyExpense;
    private Button btnTeamExpense;
    private Button btnEndTrip;

    LocationManager locationManager;
    private Context mContext;
    Dialog mDialog;
    int ii = 0;
    String tripId = "";

    private static String TAG = CurrentTripActivity.class.getSimpleName();
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_trip);

        mContext = CurrentTripActivity.this;

        tvCurrentLocation = findViewById(R.id.tvCurrentLocation);
        tvTemp = findViewById(R.id.tvTemp);
        tvWeather = findViewById(R.id.tvWeather);
        btnMyExpense = findViewById(R.id.btnMyExpense);
        btnTeamExpense = findViewById(R.id.btnTeamExpense);
        btnEndTrip = findViewById(R.id.btnEndTrip);

        tripId = PreferenceUtils.getPreference(mContext, Constant.CURRENT_TRIP);

        detectLocation();

        btnMyExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MyExpenseActivity.class);
                startActivity(intent);
            }
        });

        btnTeamExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TeamExpenseActivity.class);
                startActivity(intent);
            }
        });

        //Init Fire base
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_trips = database.getReference("trips");
        btnEndTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferenceUtils.savePreference(mContext, Constant.CURRENTLY_ON_A_TRIP, Constant.NO);
                PreferenceUtils.savePreference(mContext, Constant.CURRENT_TRIP, "");

                table_trips.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        if (dataSnapshot != null) {

                            if (ii < 1) {
                                table_trips.child(tripId).child("trip_status").setValue(Constant.COMPLETED);
                                ++ii;
                            }

                        } else {
//                            Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                Toast.makeText(mContext, "Trip ended successfully!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {

        String[] currentLocation = getLocationName(location.getLatitude(), location.getLongitude());

        if (currentLocation == null) return;

        PreferenceUtils.savePreference(mContext, "ADDRESS", currentLocation[0]);
        PreferenceUtils.savePreference(mContext, "LATITUDE", location.getLatitude() + "");
        PreferenceUtils.savePreference(mContext, "LONGITUDE", location.getLongitude() + "");

        tvCurrentLocation.setText(currentLocation[0]);
        Toast.makeText(CurrentTripActivity.this, "Location updated", Toast.LENGTH_SHORT).show();

        fetchWeatherData(currentLocation[1]);

        mDialog.dismiss();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        mDialog.dismiss();
        Toast.makeText(mContext, "Please Enable GPS and Internet", Toast.LENGTH_LONG).show();
    }

    private void detectLocation() {
        int permissionLocation = ContextCompat.checkSelfPermission(CurrentTripActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(CurrentTripActivity.this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
            getLocation();
            mDialog = ProgressDialog.show(mContext, "Fetching Location", "Please wait...", true, false);

            // Hide after some seconds
            final Handler handler = new Handler();
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                        //Toast.makeText(mContext, "Could not fetch your location!", Toast.LENGTH_LONG).show();
                    }
                }
            };

            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface mDialog) {
                    handler.removeCallbacks(runnable);
                }
            });

            handler.postDelayed(runnable, 5000);
        } else {
            getLocation();

            mDialog = ProgressDialog.show(CurrentTripActivity.this, "Fetching Location", "Please wait...", true, false);
            // Hide after some seconds
            final Handler handler = new Handler();
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (mDialog.isShowing()) {
                        mDialog.dismiss();
                        //Toast.makeText(mContext, "Could not fetch your location!", Toast.LENGTH_LONG).show();
                    }
                }
            };

            mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface mDialog) {
                    handler.removeCallbacks(runnable);
                }
            });

            handler.postDelayed(runnable, 5000);
        }
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
            //Toast.makeText(mContext, "OK ", Toast.LENGTH_SHORT).show();
        } catch (SecurityException e) {
            //Toast.makeText(mContext, "Wrong", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            Log.d("Ex", "getLocation: " + e.getMessage());
        }
    }

    private String[] getLocationName(Double latitude, Double longitude) {

        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            String location = addresses.get(0).getAddressLine(0);
            String stateName = addresses.get(0).getAddressLine(1);
            String countryName = addresses.get(0).getAddressLine(2);

            String subLocatity = addresses.get(0).getSubAdminArea();
            String locality = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();

            Log.d(TAG, "basicInfo: location = " + location);
            Log.d(TAG, "basicInfo: state = " + stateName);
            Log.d(TAG, "basicInfo: country = " + countryName);
            Log.d(TAG, "basicInfo: subLocatity = " + subLocatity);
            Log.d(TAG, "basicInfo: locality = " + locality);
            Log.d(TAG, "basicInfo: country = " + country);
            Log.d(TAG, "basicInfo: postalCode = " + postalCode);

            location = location.replace(postalCode, "")
                    .replace(locality, "")
                    .replace(country, "")
                    .replace(",", " ");

            String[] currentLocation = {location.trim(), locality.trim(), country.trim(), postalCode.trim()};

            return currentLocation;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private void fetchWeatherData(String city) {
        //progressBar.setVisibility(View.VISIBLE); //network call will start. So, show progress bar

        ApiInterface apiInterface = RetrofitApiClient.getClient().create(ApiInterface.class);

        Call<ServerResponse> call = apiInterface.getWeather(city + ",bd", "metric", "8118ed6ee68db2debfaaa5a44c832918");
        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServerResponse> call, @NonNull Response<ServerResponse> response) {

                ServerResponse serverResponse = response.body();

                if (response.code() == 200 && serverResponse != null) {

                    Temperature temperature = serverResponse.getTemperature();
                    Toast.makeText(CurrentTripActivity.this, "Weather updated", Toast.LENGTH_SHORT).show();
                    tvTemp.setText(temperature.getTemp().substring(0, 2) + "°c");

                    Temperature[] weatherList = serverResponse.getWeather();
                    //Toast.makeText(CurrentTripActivity.this, "Weather = " + weatherList[0].getDescription(), Toast.LENGTH_SHORT).show();
                    tvWeather.setText(weatherList[0].getDescription());

                } else {
                    Toast.makeText(CurrentTripActivity.this, "error " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ServerResponse> call, @NonNull Throwable t) {

                Toast.makeText(CurrentTripActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
