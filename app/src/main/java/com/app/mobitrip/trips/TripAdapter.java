package com.app.mobitrip.trips;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.trips.single_trip.TripActivity;

import java.util.List;


/**
 * Created by ASM Al-Zihadi on 5/14/2019.
 */

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.MyViewHolder> {

    int i = 0;
    Context mContext;
    private String TAG = TripAdapter.class.getSimpleName();
    private List<Trip> tripList;

    public TripAdapter(Context context, List<Trip> tripList) {
        this.tripList = tripList;
        this.mContext = context;
    }

    @Override
    public TripAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_trips_model, parent, false);

        return new TripAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TripAdapter.MyViewHolder holder, final int position) {
        final Trip single_trip = tripList.get(position);

        holder.setIsRecyclable(false);

        //try {
        //Picasso.get().load(single_trip.getImage()).into(holder.bgRestaurant);

        holder.tvTripName.setText(single_trip.getTrip_name());
        holder.cvSingleTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TripActivity.class);
                intent.putExtra("trip_id", single_trip.getTrip_id());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return tripList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvTripName;
        CardView cvSingleTrip;

        public MyViewHolder(View view) {
            super(view);

            tvTripName = view.findViewById(R.id.tvTripName);
            cvSingleTrip = view.findViewById(R.id.cvSingleTrip);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: analytics = " + getPosition());
        }
    }
}
