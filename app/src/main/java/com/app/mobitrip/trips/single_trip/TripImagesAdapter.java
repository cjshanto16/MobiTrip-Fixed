package com.app.mobitrip.trips.single_trip;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.model.TripImage;
import com.bumptech.glide.Glide;

import java.util.List;

public class TripImagesAdapter extends RecyclerView.Adapter<TripImagesAdapter.TripImageViewHolder>{

    private Context context;
    private List<TripImage> tripImages;

    public TripImagesAdapter(Context context, List<TripImage> tripImages) {
        this.context = context;
        this.tripImages = tripImages;
    }

    @NonNull
    @Override
    public TripImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_for_trip_images,parent,false);
        return new TripImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TripImageViewHolder holder, int position) {

        Glide.with(context).load(tripImages.get(position).getImage()).into(holder.tripImage);
    }

    @Override
    public int getItemCount() {
        return tripImages != null ? tripImages.size() : 0;
    }

    public class TripImageViewHolder extends RecyclerView.ViewHolder {

        private ImageView tripImage;

        public TripImageViewHolder(@NonNull View itemView) {
            super(itemView);

            tripImage = itemView.findViewById(R.id.trip_image);
        }
    }
}
