package com.app.mobitrip.trips.expense;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Expense;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

public class MyExpenseActivity extends AppCompatActivity implements ExpenseInterface {

    int ii = 0;
    private boolean isAdded = true;

    private Button btnNewExpense;
    private Button btnBack;
    private TextView tvTotal;

    private RecyclerView mRecyclerView;
    private MyExpenseAdapter mAdapter;
    public static List<Expense> myExpenses = new ArrayList<>();
    private static int myTotalExpense = 0;

    FirebaseDatabase database;
    DatabaseReference table_expenses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_expense);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        table_expenses = database.getReference("expenses");

        btnNewExpense = findViewById(R.id.btnNewExpense);
        tvTotal = findViewById(R.id.tvTotal);

        btnNewExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(MyExpenseActivity.this);
                dialog.setContentView(R.layout.dialog_add_expense);

                final MaterialEditText etSpentOn = dialog.findViewById(R.id.etSpentOn);
                final MaterialEditText etAmount = dialog.findViewById(R.id.etAmount);
                Button btnSaveExpense = dialog.findViewById(R.id.btnSaveExpense);
                btnSaveExpense.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        myExpenses.clear();
                        myTotalExpense = 0;
                        ii = 0;
                        final String spentOn = etSpentOn.getText().toString();
                        final String amount = etAmount.getText().toString();

                        if (TextUtils.isEmpty(spentOn) || TextUtils.isEmpty(spentOn)) {
                            Toast.makeText(MyExpenseActivity.this, "Fill all the fields", Toast.LENGTH_SHORT).show();
                        } else {

                            table_expenses.addValueEventListener(new ValueEventListener() {

                                final Dialog mDialog = ProgressDialog.show(MyExpenseActivity.this, "", "Please wait...", true, false);

                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    // check if user exist or not
                                    //if (dataSnapshot.child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.CURRENT_TRIP)).exists()) {
                                        /*if (isAdded) {
                                            mDialog.dismiss();
                                            Toast.makeText(MyExpenseActivity.this, "Username not available!", Toast.LENGTH_LONG).show();
                                        }*/
                                    // todo edit / add

                                    //} else {
                                    // todo add trip and then expense under it

                                    if (ii < 1) {

                                        mDialog.dismiss();

                                        Expense expense = new Expense();
                                        expense.setAmount(amount);
                                        expense.setSpentOn(spentOn);

                                        table_expenses.child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.CURRENT_TRIP))
                                                .child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.USER_NAME))
                                                .child(table_expenses.child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.CURRENT_TRIP))
                                                        .child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.USER_NAME)).push().getKey())
                                                .setValue(expense);

                                        isAdded = false;

                                        //startActivity(new Intent(MyExpenseActivity.this, MainActivity.class));
                                        /*Intent intent = new Intent(MyExpenseActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();*/
                                        Toast.makeText(MyExpenseActivity.this, "Expense Added", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                        ii = 100;
                                    }
                                    //}
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Toast.makeText(MyExpenseActivity.this, "Expense not added", Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                }
                            });

                            /*Toast.makeText(MyExpenseActivity.this, "Expense Added", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();*/

                        }
                    }
                });

                dialog.show();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        mRecyclerView = findViewById(R.id.rvMyExpenses);
        btnBack = findViewById(R.id.btnBack);


        DatabaseReference myExpenseList = table_expenses.child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.CURRENT_TRIP))
                .child(PreferenceUtils.getPreference(MyExpenseActivity.this, Constant.USER_NAME));

        myExpenseList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                myTotalExpense = 0;
                myExpenses.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    Expense expense = dataSnapshot.child(ds.getKey()).getValue(Expense.class);

                    if (expense != null) {
                        //Toast.makeText(mContext, "user now = " + PreferenceUtils.getPreference(mContext, "USER_NAME"), Toast.LENGTH_SHORT).show();

                        expense.setDataSnapshot(ds);

                        myTotalExpense += Integer.parseInt(expense.getAmount());

                        tvTotal.setText(myTotalExpense + "");
                        myExpenses.add(expense);
                        mAdapter.notifyDataSetChanged();
                    }
                }
                //Toast.makeText(mContext, dataSnapshot.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mAdapter = new MyExpenseAdapter(MyExpenseActivity.this, myExpenses, true);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyExpenseActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(MyExpenseActivity.this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onRemoveMyExpense(DataSnapshot dataSnapshot) {
        dataSnapshot.getRef().removeValue();
    }
}
