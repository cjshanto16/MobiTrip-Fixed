package com.app.mobitrip.trips.single_trip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Feed;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.model.TripImage;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TripDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    Toolbar toolbar;
    String shareId;
    TextView tvStartPoint,tvEndPoint,tvStartDate,tvTime
            ,teamSizeTxt,avgCostTxt,hotelNameTxt,busNameTxt
            ,tripNameTxt,tripDetailsTxt,fromMapTxt,toMapTxt;
    RecyclerView shareImagesRV;

    FirebaseDatabase database;
    DatabaseReference databaseReference;

    List<TripImage> tripImages;
    TripImagesAdapter tripImagesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Trip Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().getExtras()!=null)
        {
            shareId = getIntent().getExtras().getString("SHARE_ID");
        }

        tripImages = new ArrayList<>();


        tvStartPoint = findViewById(R.id.tvStartPoint);
        tvEndPoint = findViewById(R.id.tvEndPoint);
        tvStartDate = findViewById(R.id.tvStartDate);
        tvTime = findViewById(R.id.tvTime);
        teamSizeTxt = findViewById(R.id.teamSizeTxt);
        avgCostTxt = findViewById(R.id.avgCostTxt);
        hotelNameTxt = findViewById(R.id.hotelNameTXT);
        busNameTxt = findViewById(R.id.busNameTXT);
        tripNameTxt = findViewById(R.id.tripNameTxt);
        tripDetailsTxt = findViewById(R.id.tripDetailsTxt);
        fromMapTxt = findViewById(R.id.from_map);
        toMapTxt = findViewById(R.id.to_map);

        shareImagesRV = findViewById(R.id.tripDetails_imagesRV);

        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();

        databaseReference.child("feed").child(shareId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Feed feed = dataSnapshot.getValue(Feed.class);
                    tripNameTxt.setText(feed.getTrip_name());
                    tripDetailsTxt.setText(feed.getTrip_details());
                    busNameTxt.setText(feed.getBus_name());
                    hotelNameTxt.setText(feed.getHotel_name());

                    for (DataSnapshot snapshot : dataSnapshot.child("images").getChildren())
                    {
                        TripImage tripImage = snapshot.getValue(TripImage.class);
                        tripImages.add(tripImage);

                    }

                    if (tripImages != null && tripImages.size() > 0) {
                        tripImagesAdapter = new TripImagesAdapter(TripDetailsActivity.this, tripImages);
                        shareImagesRV.setLayoutManager(new LinearLayoutManager(TripDetailsActivity.this, RecyclerView.HORIZONTAL, false));
                        shareImagesRV.setAdapter(tripImagesAdapter);
                        tripImagesAdapter.notifyDataSetChanged();
                    }

                    databaseReference.child("trips").child(feed.getTrip_id()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            Trip trip = dataSnapshot.getValue(Trip.class);

                            tvStartPoint.setText(trip.getStart_point());
                            tvEndPoint.setText(trip.getEnd_point());
                            tvStartDate.setText(trip.getDate());
                            tvTime.setText(trip.getTime());

                            fromMapTxt.setText(trip.getStart_point());
                            toMapTxt.setText(trip.getEnd_point());

                            String tripMembers = trip.getTrip_members();
                            tripMembers = tripMembers.substring(1);
                            String[] members = tripMembers.split("\\+");

                            teamSizeTxt.setText(String.valueOf(members.length));

                            String perPersonAmount = trip.getAmount_per_person();
                            String commonCost = trip.getCommon_expense();

                            String avgCost = String.valueOf(Integer.parseInt(perPersonAmount)+Integer.parseInt(commonCost)/2);
                            avgCostTxt.setText(avgCost);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
