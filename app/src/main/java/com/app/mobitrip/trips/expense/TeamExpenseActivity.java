package com.app.mobitrip.trips.expense;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.Expense;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TeamExpenseActivity extends AppCompatActivity {

    private TextView tvTotal;
    private Button btnBack;
    private String currentTripId;
    private RecyclerView mRecyclerView;
    private CheckBox cbHideMyExpense;
    private LinearLayout llHideMyExpense;
    private MyExpenseAdapter mAdapter;
    public static List<Expense> teamExpenses = new ArrayList<>();
    private static int teamTotalExpense = 0;
    private int ii = 0;

    private Context mContext;
    FirebaseDatabase database;
    DatabaseReference table_expenses;
    DatabaseReference table_hide_expenses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_expense);
        mContext = this;

        currentTripId = PreferenceUtils.getPreference(mContext, Constant.CURRENT_TRIP);

        //Init Firebase
        database = FirebaseDatabase.getInstance();
        table_expenses = database.getReference("expenses");
        table_hide_expenses = database.getReference("hide_expenses");

        mRecyclerView = findViewById(R.id.rvExpenses);
        btnBack = findViewById(R.id.btnBack);
        tvTotal = findViewById(R.id.tvTotal);
        llHideMyExpense = findViewById(R.id.llHideMyExpense);
        cbHideMyExpense = findViewById(R.id.cbHideMyExpense);

        mAdapter = new MyExpenseAdapter(TeamExpenseActivity.this, teamExpenses, false);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TeamExpenseActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(TeamExpenseActivity.this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        DatabaseReference hideExpense = table_hide_expenses.child(PreferenceUtils.getPreference(TeamExpenseActivity.this, Constant.CURRENT_TRIP));
        hideExpense.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    String check = ds.getValue(String.class);
                    if(check.equalsIgnoreCase("true")){
                        cbHideMyExpense.setChecked(true);
                    }else{
                        cbHideMyExpense.setChecked(false);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference teamExpenseList = table_expenses.child(PreferenceUtils.getPreference(TeamExpenseActivity.this, Constant.CURRENT_TRIP));

        teamExpenseList.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                teamTotalExpense = 0;
                teamExpenses.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    String user = ds.getKey();

                    int mTotalExpense = 0;
                    for (DataSnapshot dp : ds.getChildren()) {

                        Expense expense = ds.child(dp.getKey()).getValue(Expense.class);

                        if (expense != null) {
                            //Toast.makeText(mContext, "user now = " + PreferenceUtils.getPreference(mContext, "USER_NAME"), Toast.LENGTH_SHORT).show();

                            mTotalExpense += Integer.parseInt(expense.getAmount());


                        }


                    }

                    Expense userExpense = new Expense();
                    userExpense.setSpentOn(user);
                    userExpense.setAmount(mTotalExpense + "");

                    teamTotalExpense += mTotalExpense;
                    teamExpenses.add(userExpense);
                    mAdapter.notifyDataSetChanged();
                    tvTotal.setText(teamTotalExpense + "");
                }
                //Toast.makeText(mContext, dataSnapshot.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Init Fire base
        //FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_trips = database.getReference("trips");
        table_trips.addValueEventListener(new ValueEventListener() {
            //final Dialog mDialog = ProgressDialog.show(mContext, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //.dismiss();

                if (dataSnapshot != null) {

                    //Log.d(TAG, "onDataChange: allUsers = " + allUsers.getKey());


                    Trip trip = dataSnapshot.child(currentTripId).getValue(Trip.class);
                    Toast.makeText(mContext, "data found ...", Toast.LENGTH_SHORT).show();


                    String current_user = PreferenceUtils.getPreference(mContext, Constant.USER_NAME);
                    String host = trip.getCreated_by();

                    if (host.equalsIgnoreCase(current_user)) {

                        llHideMyExpense.setVisibility(View.VISIBLE);

                        //isHost = true;
                        //btnStartTrip.setVisibility(View.VISIBLE);
                        /*String currentlyOnATrip = PreferenceUtils.getPreference(mContext, Constant.CURRENTLY_ON_A_TRIP);
                        if (null != currentlyOnATrip) {
                            if (currentlyOnATrip.equalsIgnoreCase(Constant.YES)) {
                                btnStartTrip.setBackground(getResources().getDrawable(R.drawable.inactive));
                            } else {
                                btnStartTrip.setBackground(getResources().getDrawable(R.drawable.save_trip));
                            }
                        }*/
                    } else {
                        llHideMyExpense.setVisibility(View.GONE);
                        //isHost = false;
                        //btnStartTrip.setVisibility(View.GONE);
                    }

                } else {

                    //Toast.makeText(mContext, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        cbHideMyExpense.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    ii = 0;

                    table_hide_expenses.addValueEventListener(new ValueEventListener() {

                        final Dialog mDialog = ProgressDialog.show(TeamExpenseActivity.this, "", "Please wait...", true, false);

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (ii < 1) {

                                mDialog.dismiss();

                                //dataSnapshot.getRef().removeValue();

                                table_hide_expenses.child(PreferenceUtils.getPreference(TeamExpenseActivity.this, Constant.CURRENT_TRIP))
                                        .setValue("true");

                                ii = 100;
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else {

                    ii = 0;

                    table_hide_expenses.addValueEventListener(new ValueEventListener() {

                        final Dialog mDialog = ProgressDialog.show(TeamExpenseActivity.this, "", "Please wait...", true, false);

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (ii < 1) {

                                mDialog.dismiss();

                                //dataSnapshot.getRef().removeValue();

                                table_hide_expenses.child(PreferenceUtils.getPreference(TeamExpenseActivity.this, Constant.CURRENT_TRIP))
                                        .setValue("false");

                                ii = 100;
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

            }
        });
    }
}
