package com.app.mobitrip.trips.single_trip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.model.TripMember;
import com.app.mobitrip.model.User;
import com.app.mobitrip.new_trip.UserAdapter;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.app.mobitrip.new_trip.NewTripActivity.userList;

public class TravelMatesActivity extends AppCompatActivity {

    public static List<User> acceptedUsers = new ArrayList<>();
    private UserAdapter userAdapter;
    private Context mContext;
    private AutoCompleteTextView acTvSearchUser;
    private CardView cvInviteMates;
    private RelativeLayout rlNoMates;
    private RecyclerView mRecyclerView;
    private AcceptedUserAdapter mAdapter;

    private String tripId;
    private boolean isHost = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_mates);

        isHost = getIntent().getBooleanExtra("host", false);
        tripId = getIntent().getStringExtra("trip_id");
        mContext = this;

        mRecyclerView = findViewById(R.id.rvSelectedUsers);
        cvInviteMates = findViewById(R.id.cvInviteMates);
        rlNoMates = findViewById(R.id.rlNoMates);

        if (!isHost) {
            cvInviteMates.setVisibility(View.GONE);
        }


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        //final DatabaseReference table_trips = database.getReference("trips");
        final DatabaseReference table_mates = database.getReference("trip_members");
        DatabaseReference mChildReference = table_mates.child(tripId);
        // todo retrieve users who accepted the invitation
        acceptedUsers.clear();
        mChildReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    User mate = dataSnapshot.child(ds.getKey()).getValue(User.class);

                    if (mate != null) {
                        //Toast.makeText(mContext, "user now = " + PreferenceUtils.getPreference(mContext, "USER_NAME"), Toast.LENGTH_SHORT).show();
                        acceptedUsers.add(mate);
                        mAdapter.notifyDataSetChanged();
                        rlNoMates.setVisibility(View.GONE);
                    }
                }
                //Toast.makeText(mContext, dataSnapshot.toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayList<String> users = new ArrayList<>();
        Log.d("", "onCreate: user number = " + userList.size());
        //Toast.makeText(mContext, "user number = "+userList.size(), Toast.LENGTH_SHORT).show();
        for (User user : userList) {
            users.add(user.getName());
            Log.d("", "onCreate: username = " + user.getName());
        }
        acTvSearchUser = findViewById(R.id.acTvSearchUser);
        userAdapter = new UserAdapter(mContext, R.layout.user_row, userList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, users);
        acTvSearchUser.setThreshold(1);
        acTvSearchUser.setAdapter(userAdapter);
        // handle click event and set desc on text view

        //Init Fire base
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_trip_members = database.getReference("trip_members");

        acTvSearchUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User user = (User) adapterView.getItemAtPosition(i);
                //fruitDesc.setText(user.getName());

                acTvSearchUser.getText().clear();
                //Toast.makeText(mContext, "user pass = "+user.getPassword(), Toast.LENGTH_SHORT).show();

                /*acceptedUsers.add(user);
                mAdapter.notifyDataSetChanged();*/

                TripMember tripMember = new TripMember();
                tripMember.setName(user.getUsername());
                tripMember.setStatus("pending");

                // to-do sent invitation
                String menu_id = table_trip_members.child(tripId).push().getKey();
                table_trip_members.child(tripId).child(menu_id).setValue(tripMember);

                Toast.makeText(mContext, "Invitation sent to " + user.getName() + ".", Toast.LENGTH_SHORT).show();
            }
        });




        if (acceptedUsers == null) {
            rlNoMates.setVisibility(View.VISIBLE);
        } else if (acceptedUsers.size() < 1) {
            rlNoMates.setVisibility(View.VISIBLE);
        } else {
            rlNoMates.setVisibility(View.GONE);
        }
        mAdapter = new AcceptedUserAdapter(mContext, acceptedUsers, isHost);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

    }
}
