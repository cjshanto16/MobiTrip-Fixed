package com.app.mobitrip.model;

import java.util.List;

public class Trip {

    private String trip_id;
    private String trip_name;
    private String start_point;
    private String end_point;
    private String date;
    private String time;
    private String amount_per_person;
    private String common_expense;
    private String created_by;
    private String trip_members;
    private String trip_status;

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public void setTrip_name(String trip_name) {
        this.trip_name = trip_name;
    }

    public String getStart_point() {
        return start_point;
    }

    public void setStart_point(String start_point) {
        this.start_point = start_point;
    }

    public String getEnd_point() {
        return end_point;
    }

    public void setEnd_point(String end_point) {
        this.end_point = end_point;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAmount_per_person() {
        return amount_per_person;
    }

    public void setAmount_per_person(String amount_per_person) {
        this.amount_per_person = amount_per_person;
    }

    public String getCommon_expense() {
        return common_expense;
    }

    public void setCommon_expense(String common_expense) {
        this.common_expense = common_expense;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getTrip_members() {
        return trip_members;
    }

    public void setTrip_members(String trip_members) {
        this.trip_members = trip_members;
    }

    public String getTrip_status() {
        return trip_status;
    }

    public void setTrip_status(String trip_status) {
        this.trip_status = trip_status;
    }
}
