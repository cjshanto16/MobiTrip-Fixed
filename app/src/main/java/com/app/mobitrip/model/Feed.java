package com.app.mobitrip.model;

import java.util.List;

public class Feed {
    private String bus_name;
    private String hotel_name;
    private String trip_details;
    private String trip_id;
    private String trip_name;
    private List<TripImage> tripImages;

    public String getBus_name() {
        return bus_name;
    }

    public void setBus_name(String bus_name) {
        this.bus_name = bus_name;
    }

    public String getHotel_name() {
        return hotel_name;
    }

    public void setHotel_name(String hotel_name) {
        this.hotel_name = hotel_name;
    }

    public String getTrip_details() {
        return trip_details;
    }

    public void setTrip_details(String trip_details) {
        this.trip_details = trip_details;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public void setTrip_name(String trip_name) {
        this.trip_name = trip_name;
    }

    public List<TripImage> getTripImages() {
        return tripImages;
    }

    public void setTripImages(List<TripImage> tripImages) {
        this.tripImages = tripImages;
    }
}
