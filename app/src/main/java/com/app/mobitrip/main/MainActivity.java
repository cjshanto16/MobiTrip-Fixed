package com.app.mobitrip.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.mobitrip.R;
import com.app.mobitrip.feed.HomeFragment;
import com.app.mobitrip.new_trip.NewTripActivity;
import com.app.mobitrip.trips.CompletedFragment;
import com.app.mobitrip.trips.TripFragment;
import com.app.mobitrip.trips.UpcomingTripFragment;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, TripFragment.OnFragmentInteractionListener,
        UpcomingTripFragment.OnFragmentInteractionListener, CompletedFragment.OnFragmentInteractionListener{

    RelativeLayout btnHome;
    RelativeLayout btnTrip;
    RelativeLayout btnNotification;
    RelativeLayout btnProfile;

    ImageView icHome;
    ImageView icTrip;
    ImageView icNotification;
    ImageView icProfile;

    public static boolean isHome = true;
    public static boolean isTrip = false;
    public static boolean isNotification = false;
    public static boolean isProfile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        icHome = findViewById(R.id.icHome);
        icTrip = findViewById(R.id.icTrip);
        icNotification = findViewById(R.id.icNotification);
        icProfile = findViewById(R.id.icProfile);

        btnHome = findViewById(R.id.btnHome);
        btnTrip = findViewById(R.id.btnTrip);
        btnNotification = findViewById(R.id.btnNotification);
        btnProfile = findViewById(R.id.btnProfile);



    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isHome){
            callHome();
        }
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isHome){
                    callHome();
                }
            }
        });

        if(isTrip){
            callTrip();
        }
        btnTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isTrip){
                    callTrip();
                }
            }
        });

        findViewById(R.id.btnNewTrip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewTripActivity.class));
            }
        });

    }

    private void callHome() {

        homeFragment();
        isHome = true;
        isTrip = false;
        isNotification = false;
        isProfile = false;

        icHome.setColorFilter(getResources().getColor(R.color.colorPrimaryDark));
        icTrip.setColorFilter(getResources().getColor(R.color.grey_50));
        icNotification.setColorFilter(getResources().getColor(R.color.grey_50));
        icProfile.setColorFilter(getResources().getColor(R.color.grey_50));
    }

    private void callTrip() {

        tripFragment();
        isHome = false;
        isTrip = true;
        isNotification = false;
        isProfile = false;

        icHome.setColorFilter(getResources().getColor(R.color.grey_50));
        icTrip.setColorFilter(getResources().getColor(R.color.colorPrimaryDark));
        icNotification.setColorFilter(getResources().getColor(R.color.grey_50));
        icProfile.setColorFilter(getResources().getColor(R.color.grey_50));
    }

    public void homeFragment() {
        Bundle bundle = new Bundle();

        Fragment fragment = new HomeFragment();
        fragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.place_holder, fragment, "home").addToBackStack("").commit();
    }

    public void tripFragment() {
        Bundle bundle = new Bundle();

        Fragment fragment = new TripFragment();
        fragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.replace(R.id.place_holder, fragment, "trips").addToBackStack("").commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
