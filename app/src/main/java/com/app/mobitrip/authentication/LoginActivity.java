package com.app.mobitrip.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.mobitrip.main.MainActivity;
import com.app.mobitrip.R;
import com.app.mobitrip.model.User;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

public class LoginActivity extends AppCompatActivity {

    private MaterialEditText etUserName;
    private MaterialEditText etPassword;
    private Button btnSignIn;
    private boolean showPass = false;
    private boolean isActive = false;
    private Context mContext;

    Boolean isRegistered = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;

        if(PreferenceUtils.getPreference(mContext, "LOGIN") != null){
            if(PreferenceUtils.getPreference(mContext, "LOGIN").equalsIgnoreCase("true")){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        findViewById(R.id.tv_sign_up_redirection).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        btnSignIn = findViewById(R.id.btnSignIn);
        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);

        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        //Toast.makeText(SignUpActivity.this, "hide", Toast.LENGTH_SHORT).show();

                        if (showPass) {
                            etPassword.setTransformationMethod(new PasswordTransformationMethod());
                            etPassword.setSelection(etPassword.getText().length());

                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visibility_off_black_18dp, 0);
                            showPass = false;
                        } else {
                            etPassword.setTransformationMethod(null);
                            etPassword.setSelection(etPassword.getText().length());
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visibility_black_18dp, 0);
                            showPass = true;
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });

        //Init Firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_users = database.getReference("users");

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                table_users.addValueEventListener(new ValueEventListener() {

                    final Dialog mDialog = ProgressDialog.show(LoginActivity.this, "", "Please wait...", true, false);

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        // check if user exist or not
                        if (dataSnapshot.child(etUserName.getText().toString()).exists()) {

                            mDialog.dismiss();
                            User user = dataSnapshot.child(etUserName.getText().toString()).getValue(User.class);
                            if (user.getPassword().equalsIgnoreCase(etPassword.getText().toString())) {

                                PreferenceUtils.savePreference(LoginActivity.this, "USER_NAME", user.getUsername());
                                PreferenceUtils.savePreference(LoginActivity.this, "LOGIN", "true");

                                Toast.makeText(LoginActivity.this, "Signed in successfully!!!", Toast.LENGTH_LONG).show();

                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, "Wrong password!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            mDialog.dismiss();
                            Toast.makeText(LoginActivity.this, "Credentials are not correct!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }

    void checkFieldsForEmptyValues() {

        boolean validationFlag[] = {false, false};

        String userName = etUserName.getText().toString();
        String password = etPassword.getText().toString();

        if (userName.equals("")) {
            validationFlag[0] = false;
        } else {
            validationFlag[0] = true;
            // todo check if unique
        }

        if (password.length() < 4) {
            validationFlag[1] = false;
        } else {
            validationFlag[1] = true;
        }

        if (validationFlag[0] && validationFlag[1] ) {
            isActive = true;
            btnSignIn.setBackground(getResources().getDrawable(R.drawable.save_trip));
        } else {
            isActive = false;
            btnSignIn.setBackground(getResources().getDrawable(R.drawable.inactive));
        }
    }
}
