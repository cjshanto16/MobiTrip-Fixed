package com.app.mobitrip.authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.app.mobitrip.main.MainActivity;
import com.app.mobitrip.R;
import com.app.mobitrip.model.User;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

public class SignUpActivity extends AppCompatActivity {

    private MaterialEditText etName;
    private MaterialEditText etEmail;
    private MaterialEditText etUserName;
    private MaterialEditText etPassword;
    private MaterialEditText etReTypePassword;
    private Button btnSignUp;
    private boolean showPass = false;
    private boolean showReTypePass = false;
    private boolean isActive = false;

    private int validCount = 0;
    Boolean isRegistered = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btnSignUp = findViewById(R.id.btnSignUp);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);

        etPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        //Toast.makeText(SignUpActivity.this, "hide", Toast.LENGTH_SHORT).show();

                        if (showPass) {
                            etPassword.setTransformationMethod(new PasswordTransformationMethod());
                            etPassword.setSelection(etPassword.getText().length());

                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visibility_off_black_18dp, 0);
                            showPass = false;
                        } else {
                            etPassword.setTransformationMethod(null);
                            etPassword.setSelection(etPassword.getText().length());
                            etPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visibility_black_18dp, 0);
                            showPass = true;
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        etReTypePassword = findViewById(R.id.etReTypePassword);

        etReTypePassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etReTypePassword.getRight() - etReTypePassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        //Toast.makeText(SignUpActivity.this, "hide", Toast.LENGTH_SHORT).show();

                        if (showReTypePass) {
                            etReTypePassword.setTransformationMethod(new PasswordTransformationMethod());
                            etReTypePassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visibility_off_black_18dp, 0);
                            showReTypePass = false;
                        } else {
                            etReTypePassword.setTransformationMethod(null);
                            etReTypePassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.visibility_black_18dp, 0);
                            showReTypePass = true;
                        }
                        return true;
                    }
                }
                return false;
            }
        });


        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });


        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });

        etReTypePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkFieldsForEmptyValues();
            }
        });

        //Init Firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_users = database.getReference("users");

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isActive) {

                    table_users.addValueEventListener(new ValueEventListener() {

                        final Dialog mDialog = ProgressDialog.show(SignUpActivity.this, "", "Please wait...", true, false);

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            // check if user exist or not
                            if (dataSnapshot.child(etUserName.getText().toString()).exists()) {
                                if (isRegistered) {
                                    mDialog.dismiss();
                                    Toast.makeText(SignUpActivity.this, "Username not available!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                mDialog.dismiss();
                                User user = new User();
                                user.setName(etName.getText().toString());
                                user.setEmail(etEmail.getText().toString());
                                user.setUsername(etUserName.getText().toString());
                                user.setPassword(etPassword.getText().toString());
                                table_users.child(etUserName.getText().toString()).setValue(user);
                                Toast.makeText(SignUpActivity.this, "Sign up successfully!!!", Toast.LENGTH_LONG).show();

                                PreferenceUtils.savePreference(SignUpActivity.this, "USER_NAME", user.getUsername());
                                PreferenceUtils.savePreference(SignUpActivity.this, "LOGIN", "true");

                                isRegistered = false;

                                //startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        findViewById(R.id.tvSignInRedirection).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    void checkFieldsForEmptyValues() {

        boolean validationFlag[] = {false, false, false, false, false};
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String userName = etUserName.getText().toString();
        String password = etPassword.getText().toString();
        String retypePassword = etReTypePassword.getText().toString();

        if (name.equals("")) {
            validationFlag[0] = false;
        } else {
            validationFlag[0] = true;
        }

        boolean validEmail1 = email.indexOf("@") != -1 ? true : false;
        boolean validEmail2 = email.indexOf(".com") != -1 ? true : false;

        if (validEmail1 && validEmail2) {
            validationFlag[1] = true;
            // todo check if unique
        } else {
            validationFlag[1] = false;
        }

        if (userName.equals("")) {
            validationFlag[2] = false;
        } else {
            validationFlag[2] = true;
            // todo check if unique
        }

        if (password.length() < 4) {
            validationFlag[3] = false;
        } else {
            validationFlag[3] = true;
        }

        if (retypePassword.equalsIgnoreCase(password)) {
            validationFlag[4] = true;
        } else {
            validationFlag[4] = false;
        }

        if (validationFlag[0] && validationFlag[1] && validationFlag[2] && validationFlag[3] && validationFlag[4]) {
            isActive = true;
            btnSignUp.setBackground(getResources().getDrawable(R.drawable.save_trip));
        } else {
            isActive = false;
            btnSignUp.setBackground(getResources().getDrawable(R.drawable.inactive));
        }
    }

}
