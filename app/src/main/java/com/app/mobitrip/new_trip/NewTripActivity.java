package com.app.mobitrip.new_trip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mobitrip.R;
import com.app.mobitrip.hotel.HotelListActivity;
import com.app.mobitrip.model.Trip;
import com.app.mobitrip.model.User;
import com.app.mobitrip.trips.single_trip.TravelMatesActivity;
import com.app.mobitrip.utils.Constant;
import com.app.mobitrip.utils.PreferenceUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import io.blackbox_vision.datetimepickeredittext.view.DatePickerEditText;
import io.blackbox_vision.datetimepickeredittext.view.TimePickerEditText;



public class NewTripActivity extends AppCompatActivity {

    private EditText tvTripName;
    private EditText tvStartPoint;
    private EditText tvEndPoint;
    private TextView tvSelectedHotel;
    private EditText etBus;
    private DatePickerEditText datePickerEditText;
    private TimePickerEditText timePickerEditText;

    private EditText tvDate;
    private MaterialEditText tvTime;
    private EditText tvAmountPerPerson;
    private EditText tvCommonExpense;
    int ii = 0;

    public static List<User> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);

        tvTripName = findViewById(R.id.tvTripName);
        tvStartPoint = findViewById(R.id.from_journey);
        tvEndPoint = findViewById(R.id.destination);
        tvSelectedHotel = findViewById(R.id.tvSelectedHotel);
        etBus = findViewById(R.id.etBus);
        //tvDate = findViewById(R.id.);
        datePickerEditText = findViewById(R.id.datePickerEditText);
        timePickerEditText = findViewById(R.id.timePickerEditText);
        tvAmountPerPerson = findViewById(R.id.amount_per);
        tvCommonExpense = findViewById(R.id.common_expense);

        tvSelectedHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // todo intent call hotel list activity
                Intent intent = new Intent(NewTripActivity.this, HotelListActivity.class);
                startActivity(intent);
            }
        });

        //Init Firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_trip = database.getReference("trips");

        findViewById(R.id.btnCreateTrip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                table_trip.addValueEventListener(new ValueEventListener() {
                    final Dialog mDialog = ProgressDialog.show(NewTripActivity.this, "", "Please wait...", true, false);

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String id = table_trip.push().getKey();

                        if (ii < 1) {
                            Trip trip = new Trip();
                            trip.setTrip_name(tvTripName.getText().toString());
                            trip.setStart_point(tvStartPoint.getText().toString());
                            trip.setEnd_point(tvEndPoint.getText().toString());
                            trip.setDate(datePickerEditText.getText().toString());
                            trip.setTime(timePickerEditText.getText().toString());
                            trip.setTrip_status(Constant.UPCOMING);
                            trip.setAmount_per_person(tvAmountPerPerson.getText().toString());
                            trip.setCommon_expense(tvCommonExpense.getText().toString());
                            trip.setCreated_by(PreferenceUtils.getPreference(NewTripActivity.this, "USER_NAME"));

                            table_trip.child(id).setValue(trip);

                            Toast.makeText(NewTripActivity.this, "Trip created Successfully!!!", Toast.LENGTH_LONG).show();

                            //hostedTrips.add(trip);
                        }
                        ii = 100;
                        mDialog.dismiss();
                        //startActivity(new Intent(NewTripActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });

        findViewById(R.id.cvTravelMates).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewTripActivity.this, TravelMatesActivity.class);
                startActivity(intent);

            }
        });

        //Init Firebase
        final DatabaseReference table_users = database.getReference("users");
        userList.clear();
        table_users.addValueEventListener(new ValueEventListener() {

            final Dialog mDialog = ProgressDialog.show(NewTripActivity.this, "", "Please wait...", true, false);

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                int i = 1;
                if (dataSnapshot != null) {
                    for (DataSnapshot allUsers : dataSnapshot.getChildren()) {

                        //Log.d(TAG, "onDataChange: allUsers = " + allUsers.getKey());

                        User user = dataSnapshot.child(allUsers.getKey()).getValue(User.class);

                        if (null != user) {

                            //user.setId(allUsers.getKey());
                            userList.add(user);


                        }
                        //Log.d(TAG, "onDataChange: restaurant ===================================== ");
                    }

                    Toast.makeText(NewTripActivity.this, "data found ...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(NewTripActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
