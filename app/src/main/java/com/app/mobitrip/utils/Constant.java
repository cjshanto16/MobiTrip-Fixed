package com.app.mobitrip.utils;

public class Constant {

    /* table names*/
    public static String TRIP = "trips";
    public static String HIDE_EXPENSE = "hide_expense";

    /* USER */
    public static String USER_NAME = "USER_NAME";

    /* trip status */
    public static String UPCOMING = "upcoming";
    public static String CURRENT = "current";
    public static String COMPLETED = "completed";

    /* trip member status*/
    public static String PENDING = "pending";
    public static String ACCEPTED = "accepted";
    public static String REJECTED = "rejected";
    public static String REMOVED = "rejected";

    /* KEY */
    public static String TRUE = "true";
    public static String FALSE = "false";
    public static String YES = "yes";
    public static String NO = "no";

    /* PreferenceUtils */
    public static String CURRENTLY_ON_A_TRIP = "currently_on_a_trip";
    public static String CURRENT_TRIP = "current_trip";

}
